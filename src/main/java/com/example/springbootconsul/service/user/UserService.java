package com.example.springbootconsul.service.user;

import com.example.springbootconsul.entiti.User;
import com.example.springbootconsul.repositories.user.UserRepositoryJPA;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserService implements MainServiceUser<User> {


    @Autowired
    UserRepositoryJPA repositoryUser;


    @Override
    public void addUserDataBaseService(User user) {
        repositoryUser.save(user);
    }



    public User findUserByLogin(String login){

        return repositoryUser.findByLogin(login);

    }


    
}
