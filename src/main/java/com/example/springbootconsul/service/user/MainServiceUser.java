package com.example.springbootconsul.service.user;



public interface MainServiceUser <User> {


    public void addUserDataBaseService(User user);
    public User findUserByLogin(String login);


}
