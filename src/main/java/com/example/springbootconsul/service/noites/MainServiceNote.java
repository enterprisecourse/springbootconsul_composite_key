package com.example.springbootconsul.service.noites;

import com.example.springbootconsul.entiti.Purchases;
import org.springframework.data.domain.Page;

import java.util.List;

public interface MainServiceNote<User, Note> {


    void addNote(Note note);

    Purchases getByIDNote(Purchases.IdPurchases idPurchases);


}
