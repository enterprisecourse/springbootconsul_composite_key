package com.example.springbootconsul.service.noites;
import com.example.springbootconsul.entiti.Purchases;
import com.example.springbootconsul.entiti.User;
import com.example.springbootconsul.repositories.note.NoteRepositoryJPA;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class NoteService implements MainServiceNote<User, Purchases> {

    @Autowired
    private final NoteRepositoryJPA noteRepositoryJPA;


    public void addNote(Purchases note) {
        noteRepositoryJPA.save(note);
    }


    @Override
    public Purchases getByIDNote(Purchases.IdPurchases idPurchases) {

        return noteRepositoryJPA.getById(idPurchases);

    }


}
