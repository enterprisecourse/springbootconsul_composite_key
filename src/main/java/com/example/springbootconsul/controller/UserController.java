package com.example.springbootconsul.controller;

import com.example.springbootconsul.dto.ResponseDto;
import com.example.springbootconsul.entiti.User;
import com.example.springbootconsul.service.user.MainServiceUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UserController {

    @Autowired
    MainServiceUser serviceUser;  // сервис для работы c пользоватенлями


    @PostMapping("addUser") // API добавить пользователя
    public void addUser(@RequestBody ResponseDto responseDto) throws Exception {
        User user = new User().setLogin(responseDto.getLogin()).setPassword(responseDto.getPassword());
        serviceUser.addUserDataBaseService(user);

    }





}
