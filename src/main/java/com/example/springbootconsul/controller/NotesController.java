package com.example.springbootconsul.controller;
import com.example.springbootconsul.dto.ResponseDto;
import com.example.springbootconsul.dto.ResponseDtoNote;
import com.example.springbootconsul.entiti.Purchases;
import com.example.springbootconsul.entiti.User;
import com.example.springbootconsul.service.noites.NoteService;
import com.example.springbootconsul.service.user.MainServiceUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@RestController
@RequiredArgsConstructor
public class NotesController {

    @Autowired
    NoteService noteService;  // сервис для работы c пользоватенлями
    @Autowired
    MainServiceUser serviceUser;  // сервис для работы c пользоватенлями


    @PostMapping("addNote") // API добавить пользователя
    public void addNote(@RequestBody ResponseDto responseDto) throws Exception {
        String login = responseDto.getLogin();
        User user = (User) serviceUser.findUserByLogin(login);
        Purchases note = new Purchases().setOrderId(responseDto.getNotesName());
        note.setDateTime(getDataTime());
        note.setUser(user);
        noteService.addNote(note);

    }

    private String getDataTime() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return now.format(formatter);
    }


    @PostMapping("getByIDNote") // API добавить пользователя
    public void getByID(@RequestBody ResponseDtoNote responseDto) throws Exception {

        Purchases.IdPurchases idPurchases = new Purchases.IdPurchases();
        idPurchases.setDateTime(responseDto.getDateTime());
        idPurchases.setOrderId(responseDto.getOrderId());

        noteService.getByIDNote(idPurchases);

    }


}
