package com.example.springbootconsul.repositories.user;


import com.example.springbootconsul.entiti.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepositoryJPA extends JpaRepository<User,String> {


    public User findByLogin(String login);


}
