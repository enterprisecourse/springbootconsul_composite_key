package com.example.springbootconsul.entiti;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import javax.persistence.*;

@Accessors(chain = true)
@Entity
@Data
@Table(name = "purchasers")
@IdClass(Purchases.IdPurchases.class)
@Getter
public class Purchases {


    @Id
    @Column(name = "order_id")
    private String orderId;

    @Id
    @Column(name = "data")
    private String dateTime;


    @OneToOne()
    @JoinColumn(name = "id_users")
    private User user;


    @Getter
    @Setter
    public static class IdPurchases implements Serializable {
        private String dateTime;
        private String orderId;
    }


}
