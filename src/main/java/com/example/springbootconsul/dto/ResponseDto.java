package com.example.springbootconsul.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Accessors
@Data
public class ResponseDto {

    private String login;
    private String password;
    private String notesName;
    private String notesDescription;


}
