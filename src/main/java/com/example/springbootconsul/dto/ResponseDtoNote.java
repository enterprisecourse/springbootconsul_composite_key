package com.example.springbootconsul.dto;


import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Accessors
@Data
public class ResponseDtoNote {

    private String dateTime;
    private String orderId;


}
